package axell.calculadoradepromedios;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


import java.lang.reflect.Array;
import java.util.Arrays;

public class calculadora extends AppCompatActivity implements TextWatcher{
    private EditText textTeorico;
    private TextView textPractico;
    private EditText primer;
    private EditText segundo;
    private EditText mejora;
    private EditText practica2;
    private  EditText teo;
    private TextView pra;
    private TextView finT;
    private  TextView minimaT;
    private TextView estadoT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);
        textTeorico= (EditText) findViewById(R.id.teorico);
        textPractico= (TextView) findViewById(R.id.practico);
        finT = (TextView) findViewById(R.id.fin);
        minimaT= (TextView) findViewById(R.id.minima);
        estadoT=(TextView) findViewById(R.id.estado);
        textTeorico.addTextChangedListener(this);



    }

    public void calcular(View view){
        teo= (EditText) findViewById(R.id.teorico);
        pra= (TextView) findViewById(R.id.practico);
        primer= (EditText) findViewById(R.id.primer);
        segundo= (EditText) findViewById(R.id.segundo);
        mejora= (EditText) findViewById(R.id.mejora);
        practica2= (EditText) findViewById(R.id.practica2);

        if (teo.length()>0 & pra.length()>0 & primer.length()>0 & segundo.length()>0 & practica2.length()>0){
            double n1= Double.parseDouble(primer.getText().toString().trim());
            double n2= Double.parseDouble(segundo.getText().toString().trim());
            double np= Double.parseDouble(practica2.getText().toString().trim());
            int nteo=Integer.parseInt(teo.getText().toString().trim());
            int npra=Integer.parseInt(pra.getText().toString().trim());
            if (mejora.length()==0){
                double n3 = 0;
                double[] notas={n1,n2,n3};
                Arrays.sort(notas);
                float cal1 = (float) notas[1];
                float cal2 = (float) notas[2];
                float suma = cal1 + cal2 ;
                float porcentaje = (float) (nteo/100.0);
                float promTeo= (float) ((suma/2.0) *porcentaje);
                float promPra= (float)(np* (npra/100.0));
                float promTotal=promTeo+promPra;
                finT.setText(promTotal+"");
                if (promTotal>=60){
                    estadoT.setText("AP");
                    minimaT.setText("0");
                }else{
                    estadoT.setText("RP");
                    float minimo = (2 * ((60-promPra)/porcentaje)) - cal2;
                    minimaT.setText(minimo+"");
                }
            }
            else{
                double n3 = Double.parseDouble(mejora.getText().toString().trim());
                double[] notas={n1,n2,n3};
                Arrays.sort(notas);
                float cal1 = (float) notas[1];
                float cal2 = (float) notas[2];
                float suma = cal1 + cal2 ;
                float porcentaje = (float) (nteo/100.0);
                float promTeo= (float) ((suma/2.0) *porcentaje);
                float porcentajePra= (float)(npra/100.0);
                float promPra= (float)(np* porcentajePra);
                float promTotal=promTeo+promPra;

                finT.setText(promTotal+"");
                if (promTotal>=60){
                    estadoT.setText("AP");
                    minimaT.setText("0");
                }else{
                    estadoT.setText("RP");
                    float minimo = (2 * ((60-promPra)/porcentaje)) - cal2;
                    minimaT.setText(minimo+"");

                }

            }

        }
        else{
            finT.setText("0");

            }




    }



    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if(textTeorico.length() > 0) {
            String valor = textTeorico.getText().toString();
            int porcentajeTeorico= Integer.parseInt(valor.trim());
            int porcentajePractico= 100-porcentajeTeorico;
            String resultado = porcentajePractico + "";
            textPractico.setText(resultado);
        }


    }
}
